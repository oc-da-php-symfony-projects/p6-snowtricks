<?php

namespace App\Tests\Controller;

use App\Entity\Image;
use App\Entity\ImageBox;
use App\Entity\Trick;
use App\Entity\Video;
use App\Entity\VideoBox;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TrickControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $client->request('GET', '/trick/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'TRICKS');
    }

    public function testShow()
    {
        $client = static::createClient();
        $client->request('GET', '/trick/new-trick-1');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'NEW TRICK 1');
    }

    public function testNew()
    {
        $trick = (new Trick('New Trick for Test', null))
            ->setHighlight(
                (new Image())->setName('new-trick-for-test_highlight')
            );
        $imageBox = (new ImageBox())->setTrick($trick)->setName('ImageBox_Trick'.$trick->getName());
        $videoBox = (new VideoBox())->setTrick($trick)->setName('videoBox_Trick'.$trick->getName());
        $trick->setImageBox($imageBox)->setVideoBox($videoBox);
        for ($i = 0; $i < 4; ++$i) {
            $image = (new Image())->setName('Image'.$i.'image_box'.$imageBox->getName());
            $imageBox->addMedium($image);
            $video = (new Video())->setName('video'.$i.'video_box'.$videoBox->getName());
            $videoBox->addMedium($video);
        }
        $this->assertTrue(4 === $trick->getImageBox()->getMedia()->count());
        $this->assertTrue(4 === $trick->getVideoBox()->getMedia()->count());
        $this->assertSame(
            'New Trick for Test',
            $trick->getName()
        );
        $this->assertSame(
            'New Trick for Test',
            $imageBox->getTrick()->getName()
        );
        $this->assertSame(
            'New Trick for Test',
            $videoBox->getTrick()->getName()
        );
        $this->assertSame(
            'ImageBox_Trick'.$trick->getName(),
            $imageBox->getName()
        );
        $this->assertSame(
            'videoBox_Trick'.$trick->getName(),
            $videoBox->getName()
        );
    }

    public function testGetEditWhileLoggedIn()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dustmslf@gmx.fr');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        // test page
        $client->request('GET', '/trick/new-trick-1/edit');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Edit Trick');
    }
}
