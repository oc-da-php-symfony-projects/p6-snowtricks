<?php

namespace App\Repository;

use App\Entity\Video;
use App\Entity\VideoBox;
use App\Service\Events\Dispatcher\VideoBoxMediaEvent;
use App\Service\VideoProvider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository
{
    private EventDispatcherInterface $dispatcher;
    private VideoProvider $provider;

    public function __construct(
        ManagerRegistry $registry,
        EventDispatcherInterface $dispatcher,
        VideoProvider $provider
    ) {
        parent::__construct($registry, Video::class);
        $this->dispatcher = $dispatcher;
        $this->provider = $provider;
    }

    public function create($newVideo)
    {
        $video = new Video();
        $this->update($video, $newVideo);
        return $video;
    }

    public function update(Video $video, $newVideo)
    {
        $infos = $this->provider->getInfos($newVideo['url']);
        $video->setName($newVideo['name']);
        $video->setUrl($newVideo['url']);
        $video->setProvidedId($infos['provided_id']);
        $video->setProvider($infos['provider']);
        $video->setThumbUrl($infos['thumb_url']);
        $video->setTitle($infos['title']);
        $video->setPlayer($infos['player']);
        $this->save($video);
        return $video;
    }

    public function save(Video $element): bool
    {
        try {
            $this->_em->persist($element);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }

        return true;
    }



    public function throwVideoBoxMediaEvent(VideoBox $videoBox, Video $video)
    {
        $this->dispatcher
            ->dispatch(new VideoBoxMediaEvent($videoBox, $video), VideoBoxMediaEvent::MEDIA_VIDEO_ADDED);
    }


    // /**
    //  * @return Video[] Returns an array of Video objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Video
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
