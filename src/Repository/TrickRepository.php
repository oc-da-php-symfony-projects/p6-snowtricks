<?php

namespace App\Repository;

use App\Entity\Trick;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Trick|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trick|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trick[]    findAll()
 * @method Trick[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrickRepository extends ServiceEntityRepository
{
    public const PAGINATOR_PER_PAGE = 15;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trick::class);
    }

//    public function getPaginator(int $offset): Paginator
//    {
//        $builder= $this->createQueryBuilder('trick')
//            ->orderBy('trick.createdAt', 'DESC')
//            ->setMaxResults(self::PAGINATOR_PER_PAGE)
//            ->setFirstResult($offset)
//        ;
//      $query =$builder->getQuery();
//        return new Paginator($query);
//    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function getPaginator(int $offset)
    {
        $builder = $this->createQueryBuilder('i')
            ->orderBy('i.createdAt', 'DESC')
            ->addOrderBy('i.lastUpdateDateTime', 'DESC');
        $builder->setFirstResult($offset)->setMaxResults(self::PAGINATOR_PER_PAGE);
        $query = $builder->getQuery();
        $paginator = new Paginator($query);
        $totalResult = $paginator->count();
        $tricks = $paginator->getIterator()->getArrayCopy();

        return ['tricks' => $tricks, 'total' => $totalResult];
    }

    // /**
    //  * @return Trick[] Returns an array of Trick objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Trick
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
