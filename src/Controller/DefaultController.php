<?php

namespace App\Controller;

use App\Repository\TrickRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private SessionInterface $session;
    private TrickRepository $trickRepository;
    protected ?Request $request;

    public function __construct(
        SessionInterface $session,
        TrickRepository $trickRepository,
        RequestStack $requestStack
    ) {
        $this->session = $session;
        $this->trickRepository = $trickRepository;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @Route({
     *     "en": "/",
     *     "fr": "/fr",
     *     "de": "/de"
     *      },
     *     name="index")
     */
    public function index(): Response
    {
        $offset = max(0, $this->request->query->getInt('offset', 0));
        $paginator = $this->trickRepository->getPaginator($offset);
        $tricks = $paginator['tricks'];
        $total = $paginator['total'];

        return $this->render('home.html.twig', [
            'tricks' => $tricks,
            'offset' => $offset,
            'total' => $total,
            'next' => min($total, $offset + TrickRepository::PAGINATOR_PER_PAGE),
        ]);
    }
}
