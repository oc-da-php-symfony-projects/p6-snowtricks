<?php

namespace App\Controller;

use App\Entity\Video;
use App\Entity\VideoBox;
use App\Form\VideoBoxType;
use App\Repository\VideoBoxRepository;
use App\Repository\VideoRepository;
use App\Service\VideoProvider;
use App\Service\VideoToBox;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route({
 *     "en":"/video-box",
 *     "fr":"/fr/video-box",
 *     "de":"/de/video-box"
 * })
 */
class VideoBoxController extends AbstractController
{
    private VideoToBox $videoToBox;
    private VideoBoxRepository $repository;
    private VideoProvider $templater;
    private ?Request $request;

    public function __construct(
        VideoToBox $videoToBox,
        VideoBoxRepository $repository,
        VideoProvider $templater,
        RequestStack $requestStack
    ) {
        $this->videoToBox = $videoToBox;
        $this->repository = $repository;
        $this->templater = $templater;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @Route("/{id}", name="video_box_show", methods={"GET"})
     */
    public function show(VideoBox $videoBox): Response
    {
        $offset = max(0, $this->request->query->getInt('offset', 0));
        $media = $videoBox->getPaginatedMedia($offset, 4);
        $total = $videoBox->getMedia()->count();

        return $this->render('video_box/show.html.twig', [
            'video_box' => $videoBox,
            'media' => $media,
            'total' => $total,
            'previous' => $offset - 1,
            'next' => min($total, $offset + 1),
            'formMediaVideo' => $this->videoToBox->form(null, $videoBox, 'new_media_video'),
        ]);
    }

    /**
     * @Route("/list/{id}", name="video_media_list", methods={"GET"})
     */
    public function getList(VideoBox $videoBox): Response
    {
        $offset = max(0, $this->request->query->getInt('offset', 0));
        $media = $videoBox->getPaginatedMedia($offset, 4);
        $total = $videoBox->getMedia()->count();

        return $this->render('video_box/_media_list.html.twig', [
            'video_box' => $videoBox,
            'media' => $media,
            'total' => $total,
            'previous' => $offset - 1,
            'next' => min($total, $offset + 1),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="video_box_edit", methods={"GET","POST"})
     */
    public function edit(VideoBox $videoBox): Response
    {
        $offset = max(0, $this->request->query->getInt('offset', 0));
        $media = $videoBox->getPaginatedMedia($offset, 4);
        $total = $videoBox->getMedia()->count();

        return $this->render('video_box/edit.html.twig', [
            'media' => $media,
            'total' => $total,
            'previous' => $offset - 2,
            'next' => min($total, $offset + 2),
            'video_box' => $videoBox,
        ]);
    }

    /**
     * @Route("/render-media-video-form/{video_box}", name="render_add_media_video_form", methods={"GET"})
     * @Route("/render-media-video-form/{video}/{video_box}", name="render_edit_media_video_form", methods={"GET"})
     */
    public function renderMediaVideoForm(?Video $video, Request $request): Response
    {
        $videoBox = $this->repository->find($request->get('video_box'));
        $mediaAction = $request->get('media_action');
        $video = !empty($video) ? $video : null;
        $formMedia = $this->videoToBox->form($video, $videoBox, $mediaAction);

        return  $this->render('video_box/_media_video_form.html.twig', [
            'media_action' => $mediaAction,
            'medium' => $video,
            'formMediaVideo' => $formMedia,
        ]);
    }

    /**
     * @Route("/{video_box_id}/new-media-video", name="new_media_video", methods={"GET","POST"})
     * @Route("/{video_box_id}/edit-media-video/{video_id}", name="edit_media_video", methods={"GET","POST"})
     */
    public function mediaVideo(Request $request, VideoToBox $videoToBox, VideoRepository $videoRepository): Response
    {
        $key = $request->request->keys()[0];
        $videoBox = $this->repository->find($request->get('video_box_id'));
        if (!empty($request->get($key)['media_video'])) {
            $video = $request->get($key)['media_video'];
            if ('edit_media_video' == $request->get('_route')) {
                $oldVideo = $videoRepository->find($request->get('video_id'));
                $videoRepository->update($oldVideo, $video);
            } else {
                $videoToBox->addMedia($videoBox, $video);
            }
        }

        return  $this->redirectToRoute('trick_add_media', ['slug' => $videoBox->getTrick()->getSlug()]);
    }

    /**
     * @Route("/{videoBox}/{video}", name="video_box_delete_media", methods={"DELETE"})
     */
    public function deleteMedia(Request $request, VideoBox $videoBox, Video $video): Response
    {
        if ($this->isCsrfTokenValid('delete'.$videoBox->getId().$video->getId(), $request->request->get('_token'))) {
            $videoBox->removeMedium($video);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($video);
            $entityManager->flush();
        }

        return $this->redirectToRoute('trick_edit', ['slug' => $videoBox->getTrick()->getSlug()]);
    }
}
