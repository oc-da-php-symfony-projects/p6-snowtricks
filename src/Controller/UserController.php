<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\User;
use App\Service\SimpleImage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

/* @Route({
     *     "en": "/user",
     *     "fr": "/fr/utilisateur",
     *     "de": "/de/user"
     *      })
     */
class UserController extends AbstractController
{
    private SimpleImage $simpleImage;

    public function __construct(SimpleImage $simpleImage)
    {
        $this->simpleImage = $simpleImage;
    }

    /**
     * @Route({
     *     "en":"/profile/{id}",
     *     "fr":"/fr/profil/{id}",
     *     "de":"/de/profile/{id}"
     *      },
     *      name="user_profile")
     */
    public function getProfile(User $user)
    {
        return $this->render('user/profile.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route({
     *     "en":"/account/{id}",
     *     "fr":"/fr/mon-compte/{id}",
     *     "de":"/de/account/{id}"
     *      },
     *      name="user_account")
     */
    public function getAccount(User $user, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $entityManager = $this->getDoctrine()->getManager();
        $formPP = $this->createFormBuilder($user)
            ->add(
                'profilePic',
                FileType::class,
                [
                'mapped' => false,
                'attr' => [
                        'accept' => 'image/jpeg, image/png',
                    ],
                'constraints' => [
                new File([
                    'maxSize' => '2M',
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                    ],
                    'mimeTypesMessage' => 'Please upload a JPG or PNG',
                ]), ], ]
            )

            ->setMethod('POST')
            ->getForm();
        $formPP->handleRequest($request);
        if ($formPP->isSubmitted() && $formPP->isValid()) {
            if ($file = $formPP['profilePic']->getData()) {
                $profilePic = new Image();
                $this->simpleImage->treat($file, $profilePic);
                $user->setProfilePic($profilePic);
                $entityManager->persist($profilePic);
            }
            $this->addFlash('success', 'Profile updated!');
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_account', ['id' => $user->getId()]);
        }

        return $this->render('user/account.html.twig', [
            'user' => $user,
            'formPP' => $formPP->createView(),
        ]);
    }
}
