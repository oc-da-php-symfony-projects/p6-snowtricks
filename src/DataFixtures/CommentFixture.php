<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Repository\TrickRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CommentFixture extends Fixture implements DependentFixtureInterface
{
    private CommentRepository $commentRepository;
    private UserRepository $userRepository;
    private TrickRepository $trickRepository;

    public function __construct(
        CommentRepository $commentRepository,
        UserRepository $userRepository,
        TrickRepository $trickRepository
    ) {
        $this->trickRepository = $trickRepository;
        $this->userRepository = $userRepository;
        $this->commentRepository = $commentRepository;
    }

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_EN');
        $tricks = $this->trickRepository->findAll();
        $users = $this->userRepository->findAll();

        foreach ($tricks as $trick) {
            $random = random_int(0, 10);
            for ($count = 0; $count < $random; ++$count) {
                shuffle($users);
                $comment = (new Comment())->setAuthor($users[0])->setTrick($trick)->setMessage($faker->text);
                $manager->persist($comment);
                $manager->flush();
            }
            $manager->persist($trick);
            $manager->flush();
        }

        $comments = $this->commentRepository->findAll();
        for ($count = 0; $count < $random; ++$count) {
            $random = random_int(0, 10);
            shuffle($users);
            shuffle($comments);
            $answer = (new Comment())
                    ->setAuthor($users[0])
                    ->setTrick($comments[0]->getTrick())
                    ->setParent($comments[0])
                    ->setMessage($faker->text);
            $manager->persist($answer);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ItemFixtures::class,
            UserFixture::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['CommentFixture'];
    }
}
