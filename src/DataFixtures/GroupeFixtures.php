<?php

namespace App\DataFixtures;

use App\Entity\Groupe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class GroupeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $groupe1 = new Groupe();
        $groupe1->setLabel('Grabs');
        $manager->persist($groupe1);
        $groupe2 = new Groupe();
        $groupe2->setLabel('Rotations');
        $manager->persist($groupe2);
        $groupe3 = new Groupe();
        $groupe3->setLabel('Flip');
        $manager->persist($groupe3);
        $groupe4 = new Groupe();
        $groupe4->setLabel('Rotations Désaxées');
        $manager->persist($groupe1);
        $groupe2 = new Groupe();
        $groupe2->setLabel('One Foot tricks');
        $manager->persist($groupe2);
        $groupe3 = new Groupe();
        $groupe3->setLabel('Slides');
        $manager->persist($groupe3);
        $manager->flush();
    }
}
