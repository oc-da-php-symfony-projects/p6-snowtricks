<?php

namespace App\Service;

use App\Entity\Image;
use App\Entity\ImageBox;
use App\Form\MediaImageBoxType;
use App\Repository\ImageRepository;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ImageToBox
{
    private ImageRepository $repository;
    private SimpleImage $simpleImage;
    private FormFactoryInterface $formFactory;
    private UrlGeneratorInterface $routeur;

    public function __construct(
        ImageRepository $repository,
        SimpleImage $simpleImage,
        FormFactoryInterface $formFactory,
        UrlGeneratorInterface $routeur
    ) {
        $this->repository = $repository;
        $this->formFactory = $formFactory;
        $this->routeur = $routeur;
        $this->simpleImage = $simpleImage;
    }

    public function form(?Image $image, ImageBox $imageBox, string $mediaAction)
    {
        $imageId = !$image ? null : $image->getId();
        $formName = uniqid('image_box_'.$imageBox->getId().'_image'.$imageId.'_');
        $form = $this->formFactory->createNamed(
            $formName,
            MediaImageBoxType::class,
            $imageBox,
            [
                'action' => $this->routeur->generate(
                    $mediaAction,
                    [
                    'image_box_id' => $imageBox->getId(),
                    'image_id' => $imageId,
                    ]
                ),
            ]
        );

        return $form->createView();
    }

    public function update(?Image $image, $imgFile)
    {
        $this->simpleImage->treat($imgFile, $image);
        $this->repository->save($image);

        return $image;
    }

    public function addMedia(ImageBox $imageBox, UploadedFile $imgFile): Image
    {
        $image = new Image();
        $this->simpleImage->treat($imgFile, $image);
        $this->repository->save($image);
        $this->repository->throwImageBoxMediaEvent($imageBox, $image);

        return $image;
    }
}
