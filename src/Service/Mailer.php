<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class Mailer
{
    private MailerInterface $mailer;
    private Environment $twig;
    private SessionInterface $session;

    public function __construct(MailerInterface $mailer, Environment $twig, SessionInterface $session)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->session = $session;
    }

    public function sendEmail(
        string $subject,
        string $from,
        string $mailto,
        string $template,
        array $param
    ) {
        $body = $this->twig->render($template, $param);
        if ('' != $body) {
            $message = (new Email())
                ->subject($subject)
                ->from(new Address($from, 'SnowTricks'))
                ->to($mailto)
                ->html($body, 'utf-8')
                ->text($body, 'utf-8');
            try {
                $this->mailer->send($message);
            } catch (TransportExceptionInterface $e) {
                $this->session->getFlashBag()->add('verified_email_error', $e->getMessage());
            }
        }
    }
}
