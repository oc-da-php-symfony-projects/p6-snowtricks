<?php


namespace App\Service\Events\Dispatcher;

use App\Entity\Video;
use App\Entity\VideoBox;
use Symfony\Contracts\EventDispatcher\Event;

class VideoBoxMediaEvent extends Event
{
    public const MEDIA_VIDEO_ADDED = 'box.addNewMediaVideo';

    private VideoBox $videoBox;
    private Video $video;

    /**
     * ImageBoxMediaEvent constructor.
     */
    public function __construct(VideoBox $videoBox, Video $video)
    {
        $this->videoBox = $videoBox;
        $this->video = $video;
    }

    /**
     * @return mixed
     */
    public function getBox()
    {
        return $this->videoBox;
    }

    /**
     * @return mixed
     */
    public function getVideo()
    {
        return $this->video;
    }
}
