<?php

namespace App\Service\Events\Listener;

use App\Repository\VideoBoxRepository;
use App\Repository\VideoRepository;
use App\Service\Events\Dispatcher\VideoBoxMediaEvent;
use Doctrine\ORM\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class VideoBoxMediaListener implements EventSubscriberInterface
{
    private VideoBoxRepository $videoBoxRepository;
    private VideoRepository $videoRepository;

    public function __construct(VideoBoxRepository $videoBoxRepository, VideoRepository $videoRepository)
    {
        $this->videoBoxRepository = $videoBoxRepository;
        $this->videoRepository = $videoRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
         VideoBoxMediaEvent::MEDIA_VIDEO_ADDED=>['onAddNewMediaVideo'],
            Events::postFlush=>['onAddNewMediaVideo']
        ];
    }

    public function onAddNewMediaVideo(VideoBoxMediaEvent $event)
    {
        $gallery = $event->getBox();
        $gallery->addMedium($event->getVideo());
        $this->videoBoxRepository->save($gallery);
    }
}
