<?php

namespace App\Service\Events\Listener;

use App\Repository\ImageBoxRepository;
use App\Repository\ImageRepository;
use App\Service\Events\Dispatcher\ImageBoxMediaEvent;
use Doctrine\ORM\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ImageBoxMediaListener implements EventSubscriberInterface
{
    private ImageBoxRepository $imageBoxRepository;
    private ImageRepository $imageRepository;

    public function __construct(ImageBoxRepository $imageBoxRepository, ImageRepository $imageRepository)
    {
        $this->imageBoxRepository = $imageBoxRepository;
        $this->imageRepository = $imageRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            ImageBoxMediaEvent::MEDIA_IMAGE_ADDED => ['onAddNewMediaImage'],
            Events::postFlush => ['onAddNewMediaImage'],
        ];
    }

    public function onAddNewMediaImage(ImageBoxMediaEvent $event)
    {
        $gallery = $event->getBox();
        $gallery->addMedium($event->getImage());
        $this->imageBoxRepository->save($gallery);
    }
}
