<?php

namespace App\Service;

use App\Entity\Image;

class SimpleImage
{
    private FileUploader $uploader;
    private GDMaker $maker;

    public function __construct(FileUploader $uploader, GDMaker $maker)
    {
        $this->uploader = $uploader;
        $this->maker = $maker;
    }

    public function treat($image_file, $image): Image
    {
        $imgFileName = $this->uploader->upload($image_file);
        $this->maker
            ->make(
                $this->maker->getUploadTargetDir().$imgFileName,
                $imgFileName
            );
        $image->setName($imgFileName)
            ->setThumbPath($this->maker->getImgPath().'thumb/thumb_'.$imgFileName)
            ->setMediumPath($this->maker->getImgPath().'medium/medium_'.$imgFileName)
            ->setOriginalPath($this->maker->getImgPath().'original/'.$imgFileName);

        return $image;
    }
}
