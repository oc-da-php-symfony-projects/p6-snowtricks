<?php

namespace App\Service;

class GDMaker
{
    private string $upload_target_dir;
    private string $img_path;
    private string $thumb_full_path;
    private string $medium_full_path;

    public function __construct(
        string $upload_target_dir,
        string $thumb_full_path,
        string $medium_full_path,
        string $img_path
    ) {
        $this->upload_target_dir = $upload_target_dir;
        $this->thumb_full_path = $thumb_full_path;
        $this->medium_full_path = $medium_full_path;
        $this->img_path = $img_path;
    }

    public function make($file, $filename)
    {
        switch (exif_imagetype($file)) {
            case IMAGETYPE_JPEG:
                $this->makejpgthumb($file, $filename);
                $this->makejpgmedium($file, $filename);
                break;
            case IMAGETYPE_PNG:
                $this->makepngthumb($file, $filename);
                $this->makepngmedium($file, $filename);
                break;
        }
    }

    public function makejpgthumb($file, $filename)
    {
        header('Content-Type: image/jpeg');

        return imagejpeg(
            imagescale(imagecreatefromjpeg($file), 250, -1),
            $this->thumb_full_path.'thumb_'.$filename,
            100
        );
    }

    public function makepngthumb($file, $filename)
    {
        header('Content-Type: image/png');

        return imagepng(
            imagescale(imagecreatefrompng($file), 250, -1),
            $this->thumb_full_path.'thumb_'.$filename,
            9
        );
    }

    public function makejpgmedium($file, $filename)
    {
        header('Content-Type: image/jpeg');

        return imagejpeg(
            imagescale(imagecreatefromjpeg($file), 1000, -1),
            $this->medium_full_path.'medium_'.$filename,
            100
        );
    }

    public function makepngmedium($file, $filename)
    {
        header('Content-Type: image/png');

        return imagepng(
            imagescale(imagecreatefrompng($file), 1000, -1),
            $this->medium_full_path.'medium_'.$filename,
            9
        );
    }

    public function getUploadTargetDir(): string
    {
        return $this->upload_target_dir;
    }

    public function getImgPath(): string
    {
        return $this->img_path;
    }

    public function getThumbFullPath(): string
    {
        return $this->thumb_full_path;
    }

    public function getMediumFullPath(): string
    {
        return $this->medium_full_path;
    }
}
