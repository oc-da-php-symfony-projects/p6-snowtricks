<?php

namespace App\Entity;

use App\Classe\MediaType;
use App\Interfaces\GalleryInterface;
use App\Interfaces\MediaTypeInterface;
use App\Repository\VideoBoxRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VideoBoxRepository::class)
 */
class VideoBox implements GalleryInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $createdAt;

    /**
     *  @ORM\ManyToMany(targetEntity="App\Entity\Video",cascade={"persist"})
     *  @ORM\JoinTable(name="videobox_video",
     *      joinColumns={@ORM\JoinColumn(name="videobox_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id",onDelete="CASCADE")}
     *      )
     */
    private Collection $media;

    /**
     * @ORM\OneToOne(targetEntity=Trick::class, inversedBy="videoBox",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Trick $trick;

    public function __construct()
    {
        $this->type = MediaType::MEDIA_VIDEO;
        $this->createdAt = new DateTime();
        $this->media = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function getMedia(): Collection
    {
        return $this->media;
    }

    public function getPaginatedMedia($offset, $limit)
    {
        $criteria = Criteria::create()
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        return $this->media->matching($criteria);
    }

    public function addMedium(MediaTypeInterface $medium): self
    {
        if (!$this->media->contains($medium)) {
            $this->media[] = $medium;
        }

        return $this;
    }

    public function removeMedium(MediaTypeInterface $medium): self
    {
        if ($this->media->contains($medium)) {
            $this->media->removeElement($medium);
        }

        return $this;
    }

    public function getTrick(): ?Trick
    {
        return $this->trick;
    }

    public function setTrick(Trick $trick): self
    {
        $this->trick = $trick;

        return $this;
    }
}
