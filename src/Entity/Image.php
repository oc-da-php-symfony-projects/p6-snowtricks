<?php

namespace App\Entity;

use App\Classe\MediaType;
use App\Interfaces\MediaTypeInterface;
use App\Repository\ImageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ImageRepository::class)
 */
class Image implements MediaTypeInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $thumbPath;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mediumPath;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $originalPath;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getThumbPath(): ?string
    {
        return $this->thumbPath;
    }

    public function setThumbPath(string $thumbPath): self
    {
        $this->thumbPath = $thumbPath;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMediumPath()
    {
        return $this->mediumPath;
    }

    /**
     * @param mixed $mediumPath
     * @return Image
     */
    public function setMediumPath($mediumPath)
    {
        $this->mediumPath = $mediumPath;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginalPath()
    {
        return $this->originalPath;
    }

    /**
     * @param mixed $originalPath
     * @return Image
     */
    public function setOriginalPath($originalPath)
    {
        $this->originalPath = $originalPath;
        return $this;
    }

    public function getType(): string
    {
        return MediaType::MEDIA_IMAGE;
    }
}
