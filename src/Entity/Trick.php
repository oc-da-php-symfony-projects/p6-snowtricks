<?php

namespace App\Entity;

use App\Repository\TrickRepository;
use App\Service\Factory\GalleryFactory;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TrickRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("slug")
 */
class Trick
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=70, unique=true)
     * @Assert\NotBlank(message = "You must give Trick a name.")
     * @Assert\Length(
     *     min = 5,
     *     max = 70,
     *     minMessage = "Too short!",
     *     maxMessage = "Too long!"
     * )
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private ?string $slug = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime" ,nullable=true)
     */
    private ?DateTime $lastUpdateDateTime = null;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="tricks")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?User $author;

    /**
     * @ORM\ManyToOne(targetEntity=Groupe::class, inversedBy="tricks")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Groupe $groupe;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Image $highlight = null;

    /**
     * @ORM\OneToOne(targetEntity=ImageBox::class,mappedBy="trick", cascade={"persist", "remove"})
     */
    private ImageBox $imageBox;

    /**
     * @ORM\OneToOne(targetEntity=VideoBox::class, mappedBy="trick", cascade={"persist", "remove"})
     */
    private VideoBox $videoBox;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="trick", orphanRemoval=true)
     */
    private Collection $comments;

    public function __construct(string $name, ?User $author)
    {
        $this->name = $name;
        $this->author = $author;
        $this->getGalleries();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function getHighlight(): ?Image
    {
        return $this->highlight;
    }

    public function setHighlight(Image $highlight): Trick
    {
        $this->highlight = $highlight;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new DateTime();
    }

    public function getLastUpdateDateTime(): ?DateTime
    {
        return $this->lastUpdateDateTime;
    }

    public function setLastUpdateDateTime(?DateTime $lastUpdateDateTime): Trick
    {
        $this->lastUpdateDateTime = $lastUpdateDateTime;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function computeSlug(SluggerInterface $slugger)
    {
//        if (!$this->slug || '^/trick/' === $this->slug) {
        $this->slug = (string) $slugger->slug((string) $this)->lower();
//        }
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setTrick($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getTrick() === $this) {
                $comment->setTrick(null);
            }
        }

        return $this;
    }

    public function getVideoBox(): ?VideoBox
    {
        return $this->videoBox;
    }

    public function setVideoBox(VideoBox $videoBox): self
    {
        $this->videoBox = $videoBox;

        // set the owning side of the relation if necessary
        if ($videoBox->getTrick() !== $this) {
            $videoBox->setTrick($this);
        }

        return $this;
    }

    public function getImageBox(): ImageBox
    {
        return $this->imageBox;
    }

    public function setImageBox(ImageBox $imageBox): Trick
    {
        $this->imageBox = $imageBox;

        return $this;
    }

    public function getGalleries()
    {
        $this->imageBox = (new GalleryFactory(ImageBox::class))->create()
            ->setTrick($this);

        $this->videoBox = (new GalleryFactory(VideoBox::class))->create()
            ->setTrick($this);
    }
}
