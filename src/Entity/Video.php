<?php

namespace App\Entity;

use App\Classe\MediaType;
use App\Interfaces\MediaTypeInterface;
use App\Repository\VideoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VideoRepository::class)
 */
class Video implements MediaTypeInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $providedId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $provider;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $thumbUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $player;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getProvidedId(): ?string
    {
        return $this->providedId;
    }

    public function setProvidedId(?string $providedId): Video
    {
        $this->providedId = $providedId;

        return $this;
    }

    public function getProvider(): ?string
    {
        return $this->provider;
    }

    public function setProvider(?string $provider): Video
    {
        $this->provider = $provider;

        return $this;
    }

    public function getThumbUrl(): ?string
    {
        return $this->thumbUrl;
    }

    public function setThumbUrl(?string $thumbUrl): Video
    {
        $this->thumbUrl = $thumbUrl;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): Video
    {
        $this->title = $title;

        return $this;
    }

    public function getPlayer(): ?string
    {
        return $this->player;
    }

    public function setPlayer(?string $player): Video
    {
        $this->player = $player;

        return $this;
    }

    public function getType(): string
    {
        return MediaType::MEDIA_VIDEO;
    }
}
