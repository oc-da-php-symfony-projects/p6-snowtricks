/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

require("popper.js");
require("jquery.easing");
require("jquery-ui");

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery'; Not working
const $ = require("jquery"); // Working
global.$ = global.jQuery = $;

// require("bootstrap");
require("bootstrap/dist/js/bootstrap");


// wysiwyg
import tinymce from "tinymce";
import "tinymce/themes/silver/index";
tinymce.init({
    selector:".trick-description"
});

$.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
}

import "./js/init";

// any CSS you import will output into a single css file (app.scss in this case)
import "./styles/app.scss";

// console.log('Hello Webpack Encore! Edit me in assets/app.old.js');
