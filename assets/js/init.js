(function ($) {
    $(".toggle-media-box").on("click",function (e) {
        e.preventDefault();
        $(".media-box").toggleClass("d-none");
    });

    $(".trick-box").on("click",".load-more-trick",function () {
        // alert('coucou');
        let self = this;
        $.ajax({
            url:this.dataset.url,
            type:"GET",
            data:{
                "offset": this.dataset.offset
            },
            success: function (data) {
                $(self).remove();
                $(".trick-box").append(data);
            }
        });
    });

    $(".image-box").on("click",".load-img",function () {
        let id = this.dataset.id;
        $.ajax({
            url:this.dataset.url,
            type:"GET",
            data:{
                "offset": this.dataset.offset
            },
            success: function (data) {
                $("#"+id).html(data);
            }
        });
    });

    $(".video-box").on("click",".load-video",function () {
        let id = this.dataset.id;
        $.ajax({
            url:this.dataset.url,
            type:"GET",
            data:{
                "offset": this.dataset.offset
            },
            success: function (data) {
                $("#"+id).html(data);
            }
        });
    });

    // $('a[id^="portfolioTriggerEdit-"]').on('click',function () {
    //     let i =  this.dataset.edit;
    //     let editId = "portfolioModalEdit-"+i;
    //     let showId = "portfolioModalShow-"+i;
    //     $('#'+showId).modal('hide');
    //     $('#'+editId).modal();
    //     $('.modal').modal('handleUpdate');
    // });

    $(".modal-close").on("click",function () {
        let modalId =  this.dataset.close;
        $("#"+modalId).modal("hide");
    });
    //
    // $('.trigger-edit-image').on('click',function () {
    //     let data = this.dataset.edit;
    //     $('#'+data+'_img_file').click();
    //     return false;
    // });


    function readUrl(input,id)
    {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $(".fileimg-"+id).attr("src", e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-img.custom-file-input").change(function () {
        let id = $(this).attr("id");
        let fileName = $(this).val().split("\\").pop();
        $(this).next(".custom-file-label").addClass("selected").html(fileName);
        readUrl(this,id);
    });

    // $('.file-img.custom-file-input').change(function () {
    //     let data = this.dataset.edit;
    //     $('#'+data).submit();
    // });


    function showACForm(commentId)
    {
        $("#answerComment"+commentId).toggleClass("d-none");
    }
    window.showACForm = showACForm;
})(jQuery,$);