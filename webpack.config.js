const Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserJsPlugin = require('terser-webpack-plugin');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // Directory where compiled assets will be stored.
    .setOutputPath('public/build/')
    // Public path used by the web server to access the output path.
    .setPublicPath('/build')
    // Only needed for CDN's or sub-directory deploy.
    .setManifestKeyPrefix('build/')
    .autoProvidejQuery()
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
    })
    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.old.js)
     * and one CSS file (e.g. app.scss) if your JavaScript imports CSS.
     */
    .addEntry(
        'fontawesomeall',
        '@fortawesome/fontawesome-free/js/all.min.js',
        {
            preload: true,
            defer: true,
            minimizer: true
        }
    )
    .addEntry(
        'app',
        './assets/app.js',
        {
            preload: true,
            defer: true,
            minimizer: true
        }
    )
    .addEntry('freelancer', './assets/js/freelancer.js')
    .addPlugin(new CopyWebpackPlugin({
        patterns: [
            { from: './assets/img', to: 'img' },
            { from: './node_modules/tinymce/skins', to: 'skins' },
            { from: './node_modules/tinymce/plugins', to: 'plugins' },
            { from: './node_modules/tinymce/icons', to: 'icons' },
        ],
    }))

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    // .splitEntryChunks()

    // Will require an extra script tag for runtime.js
    // But, you probably want this, unless you're building a single-page app.
    .enableSingleRuntimeChunk()
    .configureTerserPlugin((options)=>{
        options.cache =true;
        options.parallel = true;
        options.terserOptions = {
            output:{
                comments:false,
            },
        }
    })
    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // Enables hashed filenames (e.g. app.abc123.css).
    .enableVersioning(Encore.isProduction())

    // Enables @babel/preset-env polyfills.
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // Enables Sass/SCSS support
    .enableSassLoader()
    .sassOptions = {
        resolveUrlLoader: true,
        resolveUrlLoaderOptions: {}
    }
    // Uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // Uncomment to get integrity="..." attributes on your script & link tags
    // Requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // Uncomment if you're having problems with a jQuery plugin
    // .autoProvidejQuery()

    // Uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
    //.addEntry('admin', './assets/admin.js')
    ;

    const webpackConfig = Encore.getWebpackConfig();
    webpackConfig.plugins.push(new TerserJsPlugin());

// module.exports = Encore.getWebpackConfig();
    module.exports = webpackConfig;
